from hestia_earth.schema import EmissionMethodTier, EmissionStatsDefinition

from hestia_earth.models.log import logRequirements, logShouldRun
from hestia_earth.models.utils.emission import _new_emission
from . import MODEL

# TODO: define requirements
REQUIREMENTS = {
    "_required_type_": {

    }
}
# TODO: add lookups if any or remove line
LOOKUPS = {}
RETURNS = {
    "Emission": [{
        "value": "",
        "methodTier": "tier 1",
        "statsDefinition": "modelled"
    }]
}
TERM_ID = '_term_id_'
TIER = EmissionMethodTier._method_tier_uppercase_.value


def _emission(value: float):
    emission = _new_emission(TERM_ID, MODEL)
    emission['value'] = [value]
    emission['methodTier'] = TIER
    emission['statsDefinition'] = EmissionStatsDefinition.MODELLED.value
    return emission


def _run(_required_type_lowercase_: dict):
    # TODO: calculate emission value
    value = 0
    return [_emission(value)]


def _should_run(_required_type_lowercase_: dict):
    # TODO: define conditions to run the model

    logRequirements(_required_type_lowercase_, model=MODEL, term=TERM_ID,
                    )

    should_run = all([])
    logShouldRun(_required_type_lowercase_, MODEL, TERM_ID, should_run, methodTier=TIER)
    return should_run


def run(_required_type_lowercase_: dict):
    return _run(_required_type_lowercase_) if _should_run(_required_type_lowercase_) else []
