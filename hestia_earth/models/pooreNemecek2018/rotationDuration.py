from hestia_earth.schema import PracticeStatsDefinition

from hestia_earth.models.log import debugValues, log_as_table
from hestia_earth.models.utils.practice import _new_practice
from hestia_earth.models.utils import sum_values
from .plantationLifespan import _get_value as get_plantationLifespan
from .longFallowPeriod import _get_value as get_longFallowPeriod
from .utils import run_products_average
from . import MODEL

REQUIREMENTS = {
    "Cycle": {
        "products": [{"@type": "Product", "value": "", "term.termType": "crop"}]
    }
}
LOOKUPS = {
    "crop": ["Plantation_lifespan", "Plantation_longFallowPeriod"]
}
RETURNS = {
    "Practice": [{
        "value": "",
        "statsDefinition": "modelled"
    }]
}
TERM_ID = 'rotationDuration'


def _get_value(cycle: dict):
    def get(product: dict):
        plantationLifespan = get_plantationLifespan(product)
        longFallowPeriod = get_longFallowPeriod(product)
        product_id = product.get('term').get('@id')
        product_id_logs = log_as_table({
            'plantationLifespan': plantationLifespan,
            'longFallowPeriod': longFallowPeriod
        })
        debugValues(cycle, model=MODEL, term=TERM_ID,
                    **{product_id: product_id_logs})
        return sum_values([plantationLifespan, longFallowPeriod])
    return get


def _practice(value: float):
    practice = _new_practice(TERM_ID, MODEL)
    practice['value'] = [value]
    practice['statsDefinition'] = PracticeStatsDefinition.MODELLED.value
    return practice


def run(cycle: dict):
    value = run_products_average(cycle, TERM_ID, _get_value(cycle))
    return [_practice(value)] if value is not None else []
