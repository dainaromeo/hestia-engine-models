## Saplings

The number of saplings (young trees) produced or used per hectare per year.

### Returns

* A list of [Inputs](https://hestia.earth/schema/Input) with:
  - [term](https://hestia.earth/schema/Input#term) with [saplings](https://hestia.earth/term/saplings)
  - [methodModel](https://hestia.earth/schema/Input#methodModel) with [pooreNemecek2018](https://hestia.earth/term/pooreNemecek2018)
  - [value](https://hestia.earth/schema/Input#value)
  - [statsDefinition](https://hestia.earth/schema/Input#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `permanent pasture` **or** `permanent pasture`
  - Data completeness assessment for other: [completeness.other](https://hestia.earth/schema/Completeness#other) must be `False`
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop)

### Lookup used

- [crop.csv](https://hestia.earth/glossary/lookups/crop.csv) -> `Saplings_required`
- [other.csv](https://hestia.earth/glossary/lookups/other.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.pooreNemecek2018 import run

print(run('saplings', Cycle))
```
