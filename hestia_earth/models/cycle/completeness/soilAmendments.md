## Completeness Soil Amendments

This model checks if we have the requirements below and updates the
[Data Completeness](https://hestia.earth/schema/Completeness#soilAmendments) value.

### Returns

* A [Completeness](https://hestia.earth/schema/Completeness) with:
  - [soilAmendments](https://hestia.earth/schema/Completeness#soilAmendments)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - Data completeness assessment for soilAmendments: [completeness.soilAmendments](https://hestia.earth/schema/Completeness#soilAmendments) must be `False`
  - [endDate](https://hestia.earth/schema/Cycle#endDate)
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - a list of [measurements](https://hestia.earth/schema/Site#measurements) with:
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [soilPh](https://hestia.earth/term/soilPh)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('completeness.soilAmendments', Cycle))
```
