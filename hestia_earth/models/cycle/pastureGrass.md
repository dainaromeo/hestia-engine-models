## Pasture grass

The type of grass grown on the pasture. Data must be recorded in [key:value form](/schema/Practice#key). Use the key field to record the term describing the plant (e.g. Alfalfa plant) and the value field to record the area it covers, in percentage. If multiple grasses are grown, add the term Pasture grass multiple times, each time with a different key. The values provided should add up to 100%.

### Returns

* A [Practice](https://hestia.earth/schema/Practice) with:
  - [term](https://hestia.earth/schema/Practice#term) with [pastureGrass](https://hestia.earth/term/pastureGrass)
  - [key](https://hestia.earth/schema/Practice#key) with [genericGrassPlant](https://hestia.earth/term/genericGrassPlant)
  - [value](https://hestia.earth/schema/Practice#value) with `100`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `permanent pasture`

### Lookup used

- [landUseManagement.csv](https://hestia.earth/glossary/lookups/landUseManagement.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('pastureGrass', Cycle))
```
