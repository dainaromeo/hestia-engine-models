## Input Value

This model calculates the `value` of the [Input](https://hestia.earth/schema/Input)
by taking an average from the `min` and `max` values.

### Returns

* A list of [Inputs](https://hestia.earth/schema/Input) with:
  - [value](https://hestia.earth/schema/Input#value)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [min](https://hestia.earth/schema/Input#min) and [max](https://hestia.earth/schema/Input#max)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('input.value', Cycle))
```
