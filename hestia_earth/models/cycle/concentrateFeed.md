## Concentrate feed Properties

This model calculates all of the nutrient content values and dry matter values for a feed blend
if we know the crops that went into the blend by taking a weighted average.

### Returns

* A list of [Products](https://hestia.earth/schema/Product) with:
  - a list of [properties](https://hestia.earth/schema/Product#properties)
    - [term](https://hestia.earth/schema/Property#term) with [concentrateFeedBlend](https://hestia.earth/term/concentrateFeedBlend) **or** [concentrateFeedUnspecified](https://hestia.earth/term/concentrateFeedUnspecified)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop) **or** [animalProduct](https://hestia.earth/glossary?termType=animalProduct)
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [primary](https://hestia.earth/schema/Product#primary) = `True` and [term](https://hestia.earth/schema/Product#term) with [concentrateFeedUnspecified](https://hestia.earth/term/concentrateFeedUnspecified) **or** [concentrateFeedBlend](https://hestia.earth/term/concentrateFeedBlend)

### Lookup used

- [crop-property.csv](https://hestia.earth/glossary/lookups/crop-property.csv) -> `crudeProteinContent`
- [property.csv](https://hestia.earth/glossary/lookups/property.csv) -> `commonToSupplementInAnimalFeed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('concentrateFeed', Cycle))
```
