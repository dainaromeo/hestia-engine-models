## Cycle Pre Checks

List of models to run **before** any other model on an `Cycle`.

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import primary
from hestia_earth.models.cycle import pre_checks

cycle = pre_checks.run(cycle)
product = primary.run(cycle)
print(product)
```
