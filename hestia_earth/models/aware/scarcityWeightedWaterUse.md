## Scarcity weighted water use

This model calculates the scarcity weighted water use based on the geospatial AWARE model
(see UNEP (2016); Boulay et al (2016); Boulay et al (2020); EC-JRC (2017)).

### Returns

* A list of [Indicators](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [scarcityWeightedWaterUse](https://hestia.earth/term/scarcityWeightedWaterUse)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [aware](https://hestia.earth/term/aware)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [site](https://hestia.earth/schema/ImpactAssessment#site) with:
    - either:
      - [awareWaterBasinId](https://hestia.earth/schema/Site#awareWaterBasinId)
      - a [country](https://hestia.earth/schema/Site#country) with:
        - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)
  - optional:
    - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
      - [term](https://hestia.earth/schema/Indicator#term) with [freshwaterWithdrawalsDuringCycle](https://hestia.earth/term/freshwaterWithdrawalsDuringCycle) and [value](https://hestia.earth/schema/Indicator#value)

### Lookup used

Different lookup files are used depending on the situation:

- [awareWaterBasinId.csv](https://hestia.earth/glossary/lookups/awareWaterBasinId.csv) -> using `awareWaterBasinId` and `YR_IRRI` (for `cropland`, `glass or high accessible cover`, or `permanent pasture`) or `YR_NONIRRI`
- [region-aware-factors.csv](https://hestia.earth/glossary/lookups/region-aware-factors.csv) -> using `region` and `YR_IRRI` (for `cropland`, `glass or high accessible cover` or `permanent pasture`) or `YR_NONIRRI`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.aware import run

print(run('scarcityWeightedWaterUse', ImpactAssessment))
```
