# EMEP-EEA (2019)

These models characterise emissions according to the [EMEP/EEA air pollutant emission inventory guidebook 2019](https://www.eea.europa.eu/publications/emep-eea-guidebook-2019).
