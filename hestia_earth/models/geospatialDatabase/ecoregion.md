## Ecoregion

Ecoregions represent the original distribution of distinct assemblages of species and communities.
There are 867 terrestrial ecoregions as
[defined by WWF](https://www.worldwildlife.org/publications/terrestrial-ecoregions-of-the-world).

### Returns

- The WWF Terrestrial Ecoregion name as a `string`

### Requirements

* A [Site](https://hestia.earth/schema/Site) with:
  - either:
    - the following fields:
      - [latitude](https://hestia.earth/schema/Site#latitude)
      - [longitude](https://hestia.earth/schema/Site#longitude)
    - the following fields:
      - a [boundary](https://hestia.earth/schema/Site#boundary)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.geospatialDatabase import run

print(run('ecoregion', Site))
```
