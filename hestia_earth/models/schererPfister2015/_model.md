# Scherer Pfister (2015)

This model implements the phosphorus emissions models detailed in Scherer & Pfister (2015, Int J Life Cycle Assess, 20:785–795), many of which were originally developed in the SALCA guidelines.
