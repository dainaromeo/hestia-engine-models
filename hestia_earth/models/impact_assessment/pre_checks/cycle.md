## Impact Assessment Pre Checks Cycle

Some ImpactAssessment models need a full version of the linked [Cycle](https://hestia.earth/schema/Cycle) to run.
This model will fetch the complete version of the [Cycle](https://hestia.earth/schema/Cycle) and include it.

### Returns

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - [@id](https://hestia.earth/schema/Cycle#id) must be set (is linked to an existing Cycle)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.impact_assessment import run

print(run(ImpactAssessment))
```
