## Impact Assessment Pre Checks

List of models to run **before** any other model on an `ImpactAssessment`.

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.impact_assessment import irrigated
from hestia_earth.models.impact_assessment import pre_checks

impact = pre_checks.run(impact)
impact['irrigated'] = irrigated.run(impact)
print(impact)
```
