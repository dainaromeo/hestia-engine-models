## N2, to air, excreta

Nitrogen emissions to air, from animal excreta.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [n2ToAirExcreta](https://hestia.earth/term/n2ToAirExcreta)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [jarvisAndPain1994](https://hestia.earth/term/jarvisAndPain1994)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 1`
  - [statsDefinition](https://hestia.earth/schema/Emission#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/null#siteType) with `cropland` **or** `permanent pasture` **or** `animal housing` **or** `pond` **or** `river or stream` **or** `lake` **or** `sea or ocean`
  - a list of [emissions](https://hestia.earth/schema/Cycle#emissions) with:
    - [value](https://hestia.earth/schema/Emission#value) and [term](https://hestia.earth/schema/Emission#term) with [n2OToAirExcretaDirect](https://hestia.earth/term/n2OToAirExcretaDirect)

This model works on the following Node type with identical requirements:

* [Cycle](https://hestia.earth/schema/Cycle)
* [Transformation](https://hestia.earth/schema/Transformation)

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.jarvisAndPain1994 import run

print(run('n2ToAirExcreta', Cycle))
```
